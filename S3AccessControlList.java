package org.carlspring.cloud.storage.s3fs;
import java.nio.file.AccessDeniedException;
import java.nio.file.AccessMode;
import software.amazon.awssdk.services.s3.model.Grant;
import software.amazon.awssdk.services.s3.model.Owner;

public class S3AccessControlList
{

    private final String fileStoreName;

    private final String key;

    @Deprecated
    public S3AccessControlList(final String fileStoreName,
                               final String key,
                               final Iterable<Grant> grants,
                               final Owner owner
                               )
    {
        this.fileStoreName = fileStoreName;
        this.key = key;
    }

    public S3AccessControlList(final String fileStoreName, final String key)
    {
        this.fileStoreName = fileStoreName;
        this.key = key;
    }

    public String getKey()
    {
        return key;
    }

    public void checkAccess(final AccessMode[] modes)
            throws AccessDeniedException
    {
        for (AccessMode accessMode : modes)
        {
            if (accessMode == AccessMode.EXECUTE)
            {
                throw new AccessDeniedException(fileName(), null, "file is not executable");
            }
        }
    }

    private String fileName()
    {
        return fileStoreName + S3Path.PATH_SEPARATOR + key;
    }
}
