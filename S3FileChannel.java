package org.carlspring.cloud.storage.s3fs;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileLock;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import static java.lang.String.format;

public class S3FileChannel
        extends AsynchronousFileChannel
{

    private final Logger logger = LoggerFactory.getLogger(S3FileChannel.class);

    private final S3Path path;

    private final Set<? extends OpenOption> options;

    private final AsynchronousFileChannel fileChannel;

    private Path tempFile = null;

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    private final Lock openCloseLock = readWriteLock.writeLock();

    private final Lock writeReadChannelLock = readWriteLock.readLock();

    public S3FileChannel(final S3Path path,
                         final Set<? extends OpenOption> options,
                         final ExecutorService executor,
                         final boolean tempFileRequired)
            throws IOException
    {
        openCloseLock.lock();

        this.path = path;
        this.options = Collections.unmodifiableSet(new HashSet<>(options));
        boolean exists = path.getFileSystem().provider().exists(path);
        boolean removeTempFile = false;

        try
        {
            if (!isOpen())
            {
                if (exists && this.options.contains(StandardOpenOption.CREATE_NEW))
                {
                    throw new FileAlreadyExistsException(format("The target already exists: %s", path));
                }
                else if (!exists && !this.options.contains(StandardOpenOption.CREATE_NEW) &&
                         !this.options.contains(StandardOpenOption.CREATE))
                {
                    throw new NoSuchFileException(format("The target does not exist: %s", path));
                }

                final Set<? extends OpenOption> fileChannelOptions = new HashSet<>(this.options);
                fileChannelOptions.remove(StandardOpenOption.CREATE_NEW);

                if (tempFileRequired)
                {
                    final String key = path.getKey();
                    this.tempFile = Files.createTempFile("temp-s3-", key.replaceAll("/", "_"));
                    removeTempFile = true;

                    if (exists)
                    {
                        final S3Client client = path.getFileSystem().getClient();
                        final GetObjectRequest request = GetObjectRequest.builder()
                                                                         .bucket(path.getFileStore().name())
                                                                         .key(key)
                                                                         .build();
                        try (ResponseInputStream<GetObjectResponse> byteStream = client.getObject(request))
                        {
                            Files.copy(byteStream, tempFile, StandardCopyOption.REPLACE_EXISTING);
                        }

                        removeTempFile = false;
                    }

                    this.fileChannel = AsynchronousFileChannel.open(tempFile.toAbsolutePath(),
                                                                    fileChannelOptions,
                                                                    executor);
                }
                else
                {
                    this.tempFile = null;
                    this.fileChannel = AsynchronousFileChannel.open(path, fileChannelOptions, executor);
                }
            }
            else
            {
                throw new FileAlreadyExistsException(format("Tried to open already opened channel for path %s", path));
            }
        }
        finally
        {
            openCloseLock.unlock();

            if (removeTempFile)
            {
                Files.deleteIfExists(tempFile);
            }
        }
    }

    @Override
    public Future<Integer> read(final ByteBuffer dst,
                                final long position)
    {
        writeReadChannelLock.lock();
        try
        {
            if (isOpen())
            {
                return fileChannel.read(dst, position);
            }
            else
            {
                return CompletableFuture.completedFuture(0);
            }
        }
        finally
        {
            writeReadChannelLock.unlock();
        }
    }

    @Override
    public <A> void read(final ByteBuffer dst,
                         final long position,
                         final A attachment,
                         final CompletionHandler<Integer, ? super A> handler)
    {
        writeReadChannelLock.lock();
        try
        {
            if (isOpen())
            {
                fileChannel.read(dst, position, attachment, handler);
            }
        }
        finally
        {
            writeReadChannelLock.unlock();
        }
    }

    @Override
    public Future<Integer> write(final ByteBuffer src,
                                 final long position)
    {
        writeReadChannelLock.lock();
        try
        {
            if (isOpen())
            {
                return fileChannel.write(src, position);
            }
            else
            {
                return CompletableFuture.completedFuture(0);
            }
        }
        finally
        {
            writeReadChannelLock.unlock();
        }
    }

    @Override
    public <A> void write(final ByteBuffer src,
                          final long position,
                          final A attachment,
                          final CompletionHandler<Integer, ? super A> handler)
    {
        writeReadChannelLock.lock();
        try
        {
            if (isOpen())
            {
                fileChannel.write(src, position, attachment, handler);
            }
        }
        finally
        {
            writeReadChannelLock.unlock();
        }
    }

    @Override
    public long size()
            throws IOException
    {
        return fileChannel.size();
    }

    @Override
    public AsynchronousFileChannel truncate(final long size)
            throws IOException
    {
        return fileChannel.truncate(size);
    }

    @Override
    public void force(final boolean metaData)
            throws IOException
    {
        fileChannel.force(metaData);
    }

    @Override
    public Future<FileLock> lock(final long position,
                                 final long size,
                                 final boolean shared)
    {
        return this.fileChannel.lock(position, size, shared);
    }

    @Override
    public <A> void lock(final long position,
                         final long size,
                         final boolean shared,
                         final A attachment,
                         final CompletionHandler<FileLock, ? super A> handler)
    {
        this.fileChannel.lock(position, size, shared, attachment, handler);
    }

    @Override
    public FileLock tryLock(final long position,
                            final long size,
                            final boolean shared)
            throws IOException
    {
        return this.fileChannel.tryLock(position, size, shared);
    }

    @Override
    public boolean isOpen()
    {
        return this.fileChannel != null && this.fileChannel.isOpen();
    }

    @Override
    public void close()
            throws IOException
    {
        openCloseLock.lock();

        try
        {
            if (isOpen())
            {
                fileChannel.force(true);
                fileChannel.close();
                if (this.tempFile != null && Files.exists(tempFile))
                {
                    if (!this.options.contains(StandardOpenOption.READ))
                    {
                        sync();
                    }

                    Files.delete(tempFile);
                }
            }
            else{}

        }
        finally
        {
            openCloseLock.unlock();
        }
    }

    protected void sync()
            throws IOException
    {
        try (InputStream stream = new BufferedInputStream(Files.newInputStream(tempFile)))
        {
            final PutObjectRequest.Builder builder = PutObjectRequest.builder();
            final long length = Files.size(tempFile);
            builder.bucket(path.getFileStore().name())
                   .key(path.getKey())
                   .contentLength(length)
                   .contentType(new Tika().detect(stream, path.getFileName().toString()));

            final S3Client client = path.getFileSystem().getClient();
            client.putObject(builder.build(), RequestBody.fromInputStream(stream, length));
        }
    }
}
