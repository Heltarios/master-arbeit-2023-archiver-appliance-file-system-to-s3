

This directory contains the S3 NIO2 documentation.

## Pre-requisites

* [Docker][docker-install]
* [Docker Compose][docker-compose-install]

## start

1. `docker-compose up`
2. Open [`http://localhost:8000`](http://localhost:8000)

## Debugging 

1. Build image
   docker-compose build

2. Log into the container
   docker-compose run --rm -p 8000:8000 -v $(pwd)/../:/workspace --entrypoint /bin/sh mkdocs

2. Manually start the server (i.e. test plugins, cwd should be ./docs!)
   mkdocs serve -a 0.0.0.0:8000
